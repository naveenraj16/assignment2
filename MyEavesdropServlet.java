

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.InputStream;

import sun.net.www.URLConnection;


/**
 * Servlet implementation class MyEavesdropServlet
 */
@WebServlet("/MyEavesdropServlet")
public class MyEavesdropServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyEavesdropServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		//Step 1 read cookies from request
		
		Cookie cookies[] = request.getCookies();
		String unameCookieString = "";
		String linksCookieString = "";
		Cookie unameCookie = null;
		Cookie linksCookie = null;
		//if there already existing cookies, retrieve them and their values
		if(cookies != null){
			for (int i = 0; i < cookies.length; i++){
				Cookie cookie = cookies[i];
				//value for name cookie
				if (cookie.getName().equals("name")) {
					unameCookieString = cookie.getValue();
					unameCookie = cookie;
				} 
				//value for links cookie
				else if (cookie.getName().equals("links")) {
					linksCookieString = cookie.getValue();
					linksCookie = cookie;
				}
			}
		}
		//print website header
		out.println(
		          "<html>\n" +
		          "<head><title>MyEavesDrop </title></head>\n" +
		          "<body bgcolor=\"#f0f0f0\">\n" 
		        );
		
		//Step 2 process username and session parameters
		String username = request.getParameter("username");
		String session = request.getParameter("session");
		
		//error checks for invalid parameters
		if (((username == null) && (session != null)) || ((username != null) && (session == null))) {
			out.println("<p>Disallowed value specified for parameter username or session");
			out.println("</body>\n"+"</html>\n");
			out.close();
			return;
		}
		
		if ((username != null) && (session != null)) {
			if(username.trim().equals("")){
				out.println("<p>Disallowed value specified for parameter username");
				out.println("</body>\n"+"</html>\n");
				out.close();
				return;
			}
			if(!session.equals("start") && !session.equals("end")){
				out.println("<p>Disallowed value specified for parameter session");
				out.println("</body>\n"+"</html>\n");
				out.close();
				return;
			}
			if(session.equals("start")){
				Cookie cName = new Cookie("name",username);
				Cookie cLinks = new Cookie("links","");
				response.addCookie(cName);
				response.addCookie(cLinks);
				out.println(
				          "<P>Starting user session for user: "+username+"\n"+
				          "</body>\n"+"</html>\n"
				        );
				out.close();
				return;
			}
			//reset the cookies
			else{
				unameCookie.setMaxAge(0);
				linksCookie.setMaxAge(0);
				response.addCookie(unameCookie);
				response.addCookie(linksCookie);
				out.println(
				          "<P>Ending user session for user: "+username+"\n"+
				          "</body>\n"+"</html>\n"
				        );
				return;
			}
		}
		
		//Step 3
		//display visited urls
		out.println("Visited URLs");
		int index1 = 0;
		int index2 = linksCookieString.indexOf(',',index1);;
		while(index2 != -1){
			String s = linksCookieString.substring(index1,index2);
			//print the url
			out.println("<p>"+s);
			index1 = index2 + 1;
			index2 = linksCookieString.indexOf(',',index1);
		}
		
		
		//Step 4 Display current Requestd page (after checking for errors)
		String type = request.getParameter("type");
		String project = request.getParameter("project");
		String year = request.getParameter("year");
		
		String url = "http://eavesdrop.openstack.org/";
		//error checks for these parameters and if ok update the url
		if((type == null || (!type.equals("meetings") && !type.equals("irclogs")))){
			out.println("<p>Disallowed value specified for parameter type1");
			out.println("</body>\n"+"</html>\n");
			out.close();
			return;
		}
		if(type.equals("irclogs") && year != null){
			out.println("<p>Disallowed value specified for parameter type2");
			out.println("</body>\n"+"</html>\n");
			out.close();
			return;
		}
		url += type+"/";
		if(project != null){
			url += project+"/";
		}
		if(!type.equals("irclogs") && year == null){
			out.println("<p>Disallowed value specified for parameter year");
			out.println("</body>\n"+"</html>\n");
			out.close();
			return;
		}
		else if(type.equals("meetings") && (Integer.parseInt(year) < 2010 || Integer.parseInt(year) > 2015)){
			out.println("<p>Disallowed value specified for parameter year");
			out.println("</body>\n"+"</html>\n");
			out.close();
			return;
		}
		else if(type.equals("meetings"))
			url += year+"/";
		
		url = url.replaceAll("#","%23");
		URL u = new URL(url);
		HttpURLConnection con = (HttpURLConnection) u.openConnection();
		
		//check to see if the project given is valid
		if(con.getResponseCode() == 404){
			out.println("<p>Disallowed value specified for parameter project");
			out.println("</body>\n"+"</html>\n");
			out.close();
			return;
		}
		//update cookies
		if(unameCookie != null){
			linksCookieString += url+",";
			linksCookie = new Cookie("links",linksCookieString);
			response.addCookie(unameCookie);
			response.addCookie(linksCookie);
		}
		
		out.println("<p><p>URL data");
		
		//get the contents of the html file
	    InputStream s = con.getInputStream();
	    BufferedReader br = new BufferedReader (new InputStreamReader(s));
	    String sHtml="";
	    String line = br.readLine();
	    while (line != null) {
	      sHtml += line + "\n";
	      line = br.readLine();
	    }
	    br.close();
			
	    //extract the urls from the html string
	    int ind1 = sHtml.indexOf("Parent Directory");
        int ind2 = sHtml.indexOf("href=",ind1);
        while(ind2 != -1){
            ind1 = ind2 + 6;
            ind2 = sHtml.indexOf(">",ind1) - 1;
            String str = sHtml.substring(ind1,ind2);
            if (type.equals("meetings"))
           	   out.println("<p>http://eavesdrop.openstack.org/meetings/"+project+"/+year"+"/"+str);
            else
               out.println("<p>http://eavesdrop.openstack.org/irclogs/"+project+"/"+str);
            ind2 = sHtml.indexOf("href=",ind2);
		}
	    
	    
		out.println("</body>\n"+"</html>\n");
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
